#if defined _zcore_speed_included
 #endinput
#endif
#define _zcore_speed_included

enum SpeedRules
{
	SpeedRule_Set,
	SpeedRule_Additive,
	SpeedRule_LimitMin,
	SpeedRule_LimitMax,
	SpeedRule_Percentage,
	SpeedRule_PercentageBase
};

/* Forwards */

forward Action ZCore_Speed_OnApplyRule(int client, SpeedRules type, float amount, int immunity, float lifetime, int ladder_mode, char[] info);

/* Natives */

native int ZCore_Speed_AddClientRule(int client, SpeedRules type, float amount, int immunity, int ignore, float lifetime = 0.0, int ladder_mode, char[] info);
native int ZCore_Speed_AddTeamRule(int teamnum, SpeedRules type, float amount, int immunity, int ignore, float lifetime = 0.0, int ladder_mode, char[] info);
native int ZCore_Speed_AddRuleAll(SpeedRules type, float amount, int immunity, int ignore, float lifetime = 0.0, int ladder_mode, char[] info);

native int ZCore_Speed_RemoveRules(int client);
native int ZCore_Speed_RemoveRulesAll();
native int ZCore_Speed_RemoveRulesTeam(int teamnum);

native int ZCore_Speed_RemoveRuleByInfo(int client, char[] info);
native int ZCore_Speed_RemoveRuleByInfoAll(char[] info);
native int ZCore_Speed_RemoveRuleByInfoTeam(int teamnum, char[] info);

native int ZCore_Speed_RemoveRuleByImmunity(int client, int immunity_max);
native int ZCore_Speed_RemoveRuleByImmunityAll(int immunity_max);
native int ZCore_Speed_RemoveRuleByImmunityTeam(int teamnum, int immunity_max);

public void __pl_zcore_speed_SetNTVOptional() 
{
	MarkNativeAsOptional("ZCore_Speed_AddClientRule");
	MarkNativeAsOptional("ZCore_Speed_AddTeamRule");
	MarkNativeAsOptional("ZCore_Speed_AddRuleAll");
	
	MarkNativeAsOptional("ZCore_Speed_RemoveRules");
	MarkNativeAsOptional("ZCore_Speed_RemoveRulesAll");
	MarkNativeAsOptional("ZCore_Speed_RemoveRulesTeam");
	
	MarkNativeAsOptional("ZCore_Speed_RemoveRuleByInfo");
	MarkNativeAsOptional("ZCore_Speed_RemoveRuleByInfoAll");
	MarkNativeAsOptional("ZCore_Speed_RemoveRuleByInfoTeam");
	
	MarkNativeAsOptional("ZCore_Speed_RemoveRuleByImmunity");
	MarkNativeAsOptional("ZCore_Speed_RemoveRuleByImmunityAll");
	MarkNativeAsOptional("ZCore_Speed_RemoveRuleByImmunityTeam");
}