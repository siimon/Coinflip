#if defined _zcore_teleport_included
 #endinput
#endif
#define _zcore_teleport_included

/**
* 
* 
* 
* 
* 
* 
**/
forward Action ZCore_Teleport_OnTeleportEntity(int entity, float origin[3], float angles[3], float velocity[3], float origin_old[3], float angles_old[3], float velocity_old[3], float distance);

/**
* 
* 
* 
* 
* 
* 
**/
native bool ZCore_Teleport_TeleportEntity(int entity, float origin[3], float angles[3], float velocity[3]);

public void __pl_zcore_teleport_SetNTVOptional() 
{
	MarkNativeAsOptional("ZCore_Teleport_TeleportEntity");
}