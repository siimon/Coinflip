#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "nhnkl159"
#define PLUGIN_VERSION "1.0"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <HungerGames>
#include <colors>

// ================================================ //

// ==== ConVar's ! ==== //

ConVar gB_HGEnabled;
ConVar gB_TimerTime;
ConVar gB_CapitalTime;
ConVar gB_WinnerTime;

// ==== Int's ! ==== //
int gB_HGPlayers[MAXPLAYERS + 1];
int gB_BeamSprite = -1;
int gB_HaloSprite = -1;
int g_BeaconSerial[MAXPLAYERS+1];
int g_Serial_Gen = 0;
int PlayerKills[MAXPLAYERS + 1];
int AlliedWith[MAXPLAYERS + 1];
int UsingTeam[MAXPLAYERS+1];
int m_flNextSecondaryAttack = -1;
int gB_HNRInfected;
// ==== Char's ! ==== //
char HGWeapon[32];
HGTypes hgtype;

// ==== Float's ! ==== //
float gS_TimerTime;
float gS_TimerTime2;

// ==== Boolean's ! ==== //
bool gB_SpawnAllowed = false;
bool gS_Capitol = false;
bool gS_Winner = false;


// ================================================ //

public Plugin myinfo = 
{
	name = "[CS:GO] Jailbreak HungerGames",
	author = PLUGIN_AUTHOR,
	description = "Edited HG for Jailbreak.",
	version = PLUGIN_VERSION,
	url = "none"
};

public void OnPluginStart()
{
	// === Admin Commands === //
	RegAdminCmd("sm_hg", Cmd_HG, ADMFLAG_BAN, "Open HG Menu");
	RegAdminCmd("sm_hungergames", Cmd_HG, ADMFLAG_BAN, "Open HG Menu");
	RegAdminCmd("sm_mostkills", Cmd_Most, ADMFLAG_ROOT, "Open the most kills on HG Menu");
	RegAdminCmd("sm_restartkills", Cmd_RestartMost, ADMFLAG_ROOT, "Restart all the kills");
	
	// === Player Commands === //
	RegConsoleCmd("sm_winner", Cmd_Winner, "Open Winner Menu");
	RegConsoleCmd("sm_delteam", Cmd_DelTeam, "Delete current team (if had)");
	
	// === Events === //
	HookEvent("round_start", Event_RoundStart);
	HookEvent("round_end", Event_RoundEnd);
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_spawn", Event_PlayerSpawn);
	
	// === Shit === //
	m_flNextSecondaryAttack = FindSendPropInfo("CBaseCombatWeapon", "m_flNextSecondaryAttack");
	
	// === ConVars === //
	gB_HGEnabled = CreateConVar("hg_enabled", "0", "Is HG enabled?");
	gB_TimerTime = CreateConVar("hg_hgtimertime", "60.0", "Hungergames main timer time (In seconds).");
	gB_CapitalTime = CreateConVar("hg_capitaltime", "40.0", "Capital random slay time (in seconds).");
	gB_WinnerTime = CreateConVar("hg_winner time", "30.0", "Winner time to choose weapon (In seconds).");
}

// ==================================================================================================================================================== //

public void OnMapStart()
{
	if(gB_HGEnabled.IntValue)
	{
		gB_HGEnabled.IntValue = 0;
	}
	
	gB_BeamSprite = PrecacheModel("materials/sprites/bomb_planted_ring.vmt");
	gB_HaloSprite = PrecacheModel("materials/sprites/halo.vtf");
	PrecacheSound(HGBeacon);
	
	char download[PLATFORM_MAX_PATH];
	Format(download, sizeof(download), "sound/%s", HGStartSound);
	AddFileToDownloadsTable(download);
	PrecacheSound(HGStartSound);
	
	char download2[PLATFORM_MAX_PATH];
	Format(download2, sizeof(download2), "sound/%s", HGCanon);
	AddFileToDownloadsTable(download2);
	PrecacheSound(HGCanon);
	
	char download3[PLATFORM_MAX_PATH];
	Format(download3, sizeof(download3), "sound/%s", HGCanon);
	AddFileToDownloadsTable(download3);
	PrecacheSound(HGBloodshed);
}

// ==================================================================================================================================================== //

public void OnClientConnected(int client)
{
	if(IsValidClient(client))
	{
		SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
		SDKHook(client, SDKHook_PreThink, OnPreThink);
		
		PlayerKills[client] = 0;
	}
}
public void OnClientDisconnected(int client)
{
	if(IsValidClient(client))
	{
		SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
		SDKUnhook(client, SDKHook_PreThink, OnPreThink);
		
		PlayerKills[client] = 0;
	}
}

public Action OnTakeDamage(int client, int &attacker, int &inflictor, float &damage, int &damagetype)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Continue;
	}
	
	if(hgtype == HG_HS)
	{
		if(damagetype & CS_DMG_HEADSHOT)
		{
			damage = 100.0;
			return Plugin_Changed;
		}
		else
		{
			damage = 0.0;
			return Plugin_Changed;
		}
	}
	if(hgtype == HG_JumpShots)
	{
		int iGroundEntity = GetEntPropEnt(attacker, Prop_Send, "m_hGroundEntity");
		if(iGroundEntity != -1)
		{
			damage = 0.0;
			return Plugin_Changed;
		}
	}
	
	if(hgtype == HG_HAR)
	{
		if(attacker != gB_HNRInfected)
		{
			damage = 0.0;
			return Plugin_Changed;
		}
		SetEntityRenderMode(attacker, RENDER_NORMAL);
		SetEntityRenderColor(attacker);
			
		gB_HNRInfected = client;
			
		SetEntityRenderMode(client, RENDER_TRANSALPHA);
		SetEntityRenderColor(client, GetRandomInt(1, 255), GetRandomInt(1, 255), GetRandomInt(1, 255));
			
		CPrintToChatAll("%s \x07%N\x01 hit \x07%N\x1 and he is now the infected !", PREFIX, attacker, client);
		damage = 0.0;
		return Plugin_Changed;
	}
	
	if(UsingTeam[client] == 0)
	{
		return Plugin_Continue;
	}
	
	if(attacker == AlliedWith[client] && client == AlliedWith[attacker])
	{
		damage = 0.0;
		return Plugin_Changed;
	}
	
	return Plugin_Continue;
}

public Action OnPreThink(int client)
{
	if(!IsValidClient(client))
	{
		return Plugin_Continue;
	}
	if (hgtype == HG_Noscope)
	{
		int iWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
		SetNoScope(iWeapon);
	}
	if (hgtype == HG_HAR)
	{
		int iWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
		SetNoScope(iWeapon);
	}
	return Plugin_Continue;
}

stock void SetNoScope(int weapon)
{
	if (IsValidEdict(weapon))
	{
		char classname[MAX_NAME_LENGTH];
		GetEdictClassname(weapon, classname, sizeof(classname));
		
		if (StrEqual(classname[7], "ssg08") || StrEqual(classname[7], "aug") || StrEqual(classname[7], "sg550") || StrEqual(classname[7], "sg552") || StrEqual(classname[7], "sg556") || StrEqual(classname[7], "awp") || StrEqual(classname[7], "scar20") || StrEqual(classname[7], "g3sg1"))
			SetEntDataFloat(weapon, m_flNextSecondaryAttack, GetGameTime() + 2.0);
	}
}

public Action KillInfected_Timer(Handle timer)
{
	if(hgtype != HG_HAR)
	{
		return Plugin_Stop;
	}
	if(gS_Winner)
	{
		return Plugin_Stop;
	}
	
	if((GetEngineTime() - gS_TimerTime2) > 30.0)
	{
		if (!IsValidClient(gB_HNRInfected)) return Plugin_Continue;
		CPrintToChatAll("%s \x07%N\x01 dead because he didnt spread the infection !", PREFIX, gB_HNRInfected);
		ForcePlayerSuicide(gB_HNRInfected);
		
		return Plugin_Stop;
	}
	else
	{
		char sHintText[1000];
		Format(sHintText, sizeof(sHintText), "Time left : %.02f  \nInfected : %N ", (gS_TimerTime2 + 30.0) - GetEngineTime(), gB_HNRInfected);
		PrintHintTextToAll(sHintText);
		
		SetEntityRenderMode(gB_HNRInfected, RENDER_TRANSALPHA);
		SetEntityRenderColor(gB_HNRInfected, GetRandomInt(1, 255), GetRandomInt(1, 255), GetRandomInt(1, 255));
	}
	
	return Plugin_Continue;
}


// ==================================================================================================================================================== //

public void Event_RoundStart(Event e, const char[] name, bool dontBroadcast)
{
	if(!gB_HGEnabled.IntValue)
	{
		return;
	}
	
	if(hgtype == HG_None)
	{
		hgtype = HG_Normal;
	}
	

	HG_LoopClients(i)
	{
		if(IsValidClient(i))
		{
			PlaySound(i, HGStartSound);
		}
	}
	
	gS_TimerTime = GetEngineTime();
	CreateTimer(0.1, HG_Timer, _, TIMER_REPEAT);
	
	
	switch(hgtype)
	{
		case HG_Dodgeball:
		{
			HG_LoopClients(i)
			{
				if(IsValidClient(i) && IsPlayerAlive(i))
				{
					SetEntityHealth(i, 1);
				}
			}
			CreateTimer(1.0, CheckDodgeBallCheaters, _, TIMER_REPEAT);
		}
	}
	
	
	CPrintToChatAll("%s Current HG mode : \x07%s", PREFIX, HGTypesNames[view_as<int>(hgtype)]);

	if(GetAliveCT() > 0)
	{
		CheckForCT();
	}
	SetConVarInt(FindConVar("mp_friendlyfire"), 0);
	SetConVarInt(FindConVar("mp_teammates_are_enemies"), 0);
}

void CheckForCT()
{
	HG_LoopClients(i)
	{
		if(IsValidClient(i) && IsPlayerAlive(i) && GetClientTeam(i) == 3)
		{
			ChangeClientTeam(i, 2);
			CS_RespawnPlayer(i);
		}
	}
}

public void Event_RoundEnd(Event e, const char[] name, bool dontBroadcast)
{
	if(!gB_HGEnabled.IntValue)
	{
		return;
	}
	
	for(int i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
		{
			AlliedWith[i] = 0;
			UsingTeam[i] = 0;
		}
	}
	
	gB_SpawnAllowed = true;
	gS_Winner = false;
}

// ==================================================================================================================================================== //

public void Event_PlayerDeath(Event e, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(e, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(e, "attacker"));
	
	if(!gB_HGEnabled.IntValue)
	{
		return;
	}
	
	if(!IsValidClient(client))
	{
		return;
	}
	
	HG_LoopClients(i)
	{
		if(IsValidClient(i))
		{
			PlaySound(i, HGCanon);
		}
	}
	
	if(hgtype == HG_HAR)
	{
		if(client == gB_HNRInfected)
		{
			CreateTimer(5.0, NewInfected_Timer);
			CPrintToChatAll("%s New infected will get picked in 5 seconds...", PREFIX);
		}
	}
	
	if(g_BeaconSerial[client] != 0)
	{
		KillBeacon(client);
	}
	
	if(GetAliveT() == 3)
	{
		HG_LoopClients(i)
		{
			if(IsValidClient(i) && IsPlayerAlive(i))
			{
				CreateBeacon(i);
			}
		}
	}
	if(GetAliveT() == 2)
	{
		gS_Capitol = true;
		gS_TimerTime2 = GetEngineTime();
		CreateTimer(0.1, Capitol_Timer, _, TIMER_REPEAT);
	}
	if(GetAliveT() == 1)
	{
		gS_Capitol = false;
		gS_Winner = true;
		KillAllBeacons();
		HG_LoopClients(i)
		{
			if(IsValidClient(i) && IsPlayerAlive(i))
			{
				gB_HGPlayers[HGWinner] = i;
				FakeClientCommand(i, "sm_winner");
				PrintToChatAll("%s HungerGames winner for this round is : \x07%N", PREFIX, i);
			}
		}
		gS_TimerTime2 = GetEngineTime();
		CreateTimer(0.1, Winner_Timer, client, TIMER_REPEAT);
	}
	
	if(client == attacker)
	{
		return;
	}
	PlayerKills[attacker]++;
	CPrintToChat(attacker, "%s You current kills : \x07%d", PREFIX, PlayerKills[attacker]);
}

public Action Capitol_Timer(Handle timer)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Stop;
	}
	if(!gS_Capitol)
	{
		return Plugin_Stop;
	}
	if((GetEngineTime() - gS_TimerTime2) > gB_CapitalTime.FloatValue)
	{
		int RandomPlayer = GetRandomPlayer();
		
		ForcePlayerSuicide(RandomPlayer);
		CPrintToChatAll("%s \x07%N\x1 was picked randomaly and got slayed !", PREFIX, RandomPlayer);
		return Plugin_Stop;
	}
	else
	{
		PrintHintTextToAll("The capital will slay random player in : <b> %.02f </b>", (gS_TimerTime2 + gB_CapitalTime.FloatValue) - GetEngineTime());
	}
	
	return Plugin_Continue;
}

public Action Winner_Timer(Handle timer, any client)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Stop;
	}
	if(!gS_Winner)
	{
		return Plugin_Stop;
	}
	if((GetEngineTime() - gS_TimerTime2) > gB_WinnerTime.FloatValue)
	{
		ForcePlayerSuicide(client);
		CPrintToChatAll("%s \x07%N\x1 was slayed cause he didnt picked weapon on time !", PREFIX, client);
		return Plugin_Stop;
	}
	else
	{
		PrintHintTextToAll("The capital will slay random player in : <b> %.02f </b>", (gS_TimerTime2 + gB_WinnerTime.FloatValue) - GetEngineTime());
	}
	
	return Plugin_Continue;
}

stock void CreateBeacon(int client)
{
	g_BeaconSerial[client] = ++g_Serial_Gen;
	CreateTimer(1.0, Timer_Beacon, client | (g_Serial_Gen << 7), TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);	
}

stock void KillBeacon(int client)
{
	g_BeaconSerial[client] = 0;

	if (IsValidClient(client))
	{
		SetEntityRenderColor(client, 255, 255, 255, 255);
	}
}

stock void KillAllBeacons()
{
	for (int i = 1; i <= MaxClients; i++)
	{
		KillBeacon(i);
	}
}

public Action Timer_Beacon(Handle Timer, any value)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Continue;
	}
	
	int client = value & 0x7f;
	int serial = value >> 7;
	
	if(g_BeaconSerial[client] != serial || !IsValidClient(client))
	{
		return Plugin_Continue;
	}
	
	int Color[4];
	Color[0] = GetRandomInt(0, 255);
	Color[1] = GetRandomInt(0, 255);
	Color[2] = GetRandomInt(0, 255);
	Color[3] = GetRandomInt(190, 255); 

	float vec[3];
	GetClientAbsOrigin(client, vec);
	vec[2] += 10;
	
	TE_SetupBeamRingPoint(vec, 10.0, 250.0, gB_BeamSprite, gB_HaloSprite, 0, 15, 0.5, 2.0, 0.0, Color, 10, 0);
	TE_SendToAll();
	
	GetClientEyePosition(client, vec); 
	for (int i = 0; i <= MaxClients; i++)
	{
		if(IsValidClient(i))
		{
			PlaySound(i, HGBeacon);
		}
	}
	return Plugin_Continue;
}

// ==================================================================================================================================================== //

public void Event_PlayerSpawn(Event e, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(e, "userid"));
	
	if(!gB_HGEnabled.IntValue)
	{
		return;
	}
	
	if(!IsValidClient(client))
	{
		return;
	}
	
	if(!gB_SpawnAllowed)
	{
		CreateTimer(1.0, SlayLate_Timer, client);
	}
	
	RequestFrame(RemoveRadar, client);
}

public void RemoveRadar(any client)
{
	SetEntProp(client, Prop_Send, "m_iHideHUD", GetEntProp(client, Prop_Send, "m_iHideHUD") | HIDE_RADAR_CSGO);
}

public Action SlayLate_Timer(Handle timer, any client)
{
	ForcePlayerSuicide(client);
}

// ==================================================================================================================================================== //


public Action CheckDodgeBallCheaters(Handle timer)
{
	if(hgtype != HG_Dodgeball)
	{
		return Plugin_Stop;
	}
	
	HG_LoopClients(i)
	{
		if(IsValidClient(i) && IsPlayerAlive(i) && GetClientHealth(i) > 1)
		{
			SetEntityHealth(i, 1);
		}
	}
	
	return Plugin_Continue;
}

public Action HG_Timer(Handle timer)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Stop;
	}
	
	
	if((GetEngineTime() - gS_TimerTime) > gB_TimerTime.FloatValue)
	{
		char HGStart[128];
		Format(HGStart, 128, "The HungerGames has started !");
		SendPanelToAll("===========\n  HungerGames    \n===========", HGStart);
		PrintHintTextToAll(HGStart);
		
		CPrintToChatAll("%s Friendly fire : \x07Disabled \x01-> \x05Enabled !", PREFIX);
		
		//Started Cvars and shit
		SetConVarInt(FindConVar("mp_friendlyfire"), 1);
		SetConVarInt(FindConVar("mp_teammates_are_enemies"), 1);
		gB_SpawnAllowed = false;
		HG_LoopClients(i)
		{
			if(IsValidClient(i))
			{
				PlaySound(i, HGBloodshed);
			}
		}
		if(hgtype == HG_Normal || hgtype == HG_HS || hgtype == HG_JumpShots)
		{
			GiveWeaponAll();
		}
		if(hgtype == HG_Dodgeball)
		{
			HG_LoopClients(i)
			{
				if(IsValidClient(i) && IsPlayerAlive(i))
				{
					GivePlayerItem(i, "weapon_flashbang");
				}
			}
		}
		if(hgtype == HG_Noscope)
		{
			HG_LoopClients(i)
			{
				if(IsValidClient(i) && IsPlayerAlive(i))
				{
					GivePlayerItem(i, "weapon_scout");
				}
			}
		}
		if(hgtype == HG_HAR)
		{
			gB_HNRInfected = GetRandomPlayer();
			GivePlayerItem(gB_HNRInfected, "weapon_ssg08");
			
			gS_TimerTime2 = GetEngineTime();
			CreateTimer(0.1, KillInfected_Timer, _, TIMER_REPEAT);
		}
		
		gS_TimerTime = GetEngineTime();
		CreateTimer(1.0, CamperSlay_Timer, _, TIMER_REPEAT);
		
		return Plugin_Stop;
	}
	else
	{
		char HGTimer[128];
		Format(HGTimer, 128, "The HungerGames will start in : %.02f", (gS_TimerTime + gB_TimerTime.FloatValue) - GetEngineTime());
		SendPanelToAll("===========\n  HungerGames    \n===========", HGTimer);
		PrintHintTextToAll(HGTimer);
	}
	
	
	return Plugin_Continue;
}
public Action NewInfected_Timer(Handle timer)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Stop;
	}
	gB_HNRInfected = GetRandomPlayer();
	gS_TimerTime2 = GetEngineTime();
	CreateTimer(0.1, KillInfected_Timer, _, TIMER_REPEAT);
	return Plugin_Continue;
}

void GiveWeaponAll()
{
	HG_LoopClients(i)
	{
		if(IsValidClient(i) && IsPlayerAlive(i))
		{
			GivePlayerItem(i, HGWeapon);
			CPrintToChat(i, "%s Current HG weapon : \x07%s", PREFIX, HGWeapon);
		}
	}
}

stock void GetRandomWeapon()
{
	int rand = GetRandomInt(0, sizeof(WeaponsAliasForMenu) - 1);
	int randomweap = GetRandomInt(0, 4);
	
	Format(HGWeapon, sizeof(HGWeapon), WeaponsAliasForMenu[rand][randomweap]);
}

public Action CamperSlay_Timer(Handle timer)
{
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Stop;
	}
	
	if((GetEngineTime() - gS_TimerTime) > 120.0)
	{
		char CamperSlay[128];
		Format(CamperSlay, 128, "17:00 !!! \n Terrorist camper == slay.");
		SendPanelToAll("===========\n  HungerGames    \n===========", CamperSlay);
		PrintHintTextToAll(CamperSlay);
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

// ==================================================================================================================================================== //

public int HGMenuHandler(Menu menu, MenuAction action, int client, int item)
{
	if(action == MenuAction_Select)
	{
		char choice[32];
		menu.GetItem(item, choice, sizeof(choice));
		if(StrEqual(choice, "enable"))
		{
			gB_SpawnAllowed = true;
			gB_HGEnabled.IntValue = 1;
			SetConVarInt(FindConVar("mp_weapons_allow_map_placed"), 0);
			SetConVarInt(FindConVar("mp_restartgame"), 3);
			CPrintToChatAll("%s The Hungergames has been \x07enabled !", PREFIX);
			GetRandomWeapon();
		}
		if(StrEqual(choice, "disable"))
		{
				
			gB_HGEnabled.IntValue = 0;
			SetConVarInt(FindConVar("mp_weapons_allow_map_placed"), 1);
			hgtype = HG_None;
			SetConVarInt(FindConVar("mp_restartgame"), 3);
			CPrintToChatAll("%s The Hungergames has been \x07disabled !", PREFIX);
		}
	}
	if(action == MenuAction_Cancel)
	{
		delete menu;
	}
	if(action == MenuAction_End)
	{
		delete menu;
	}
}

public Action Cmd_HG(int client, int args)
{
	if(!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	Menu menu = new Menu(HGMenuHandler);
	menu.SetTitle("HungerGames menu :");
	menu.AddItem("enable", "Start The HG");
	menu.AddItem("disable", "Stop The HG");
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}

// ==================================================================================================================================================== //

public int WinnerMenuHandler(Menu menu, MenuAction action, int client, int item)
{
	if(action == MenuAction_Select)
	{
		if(!IsValidClient(client))
		{
			return 0;
		}
		if(!gB_HGEnabled.IntValue)
		{
			return 0;
		}
		if(!IsPlayerAlive(client))
		{
			CPrintToChat(client, "%s In order to use this command , you must be alive !");
			return 0;
		}
		if(GetAliveT() != 1)
		{
			CPrintToChat(client, "%s In order to use this command , there must an HG winner !");
			return 0;
		}
		if(client != gB_HGPlayers[HGWinner])
		{
			CPrintToChat(client, "%s In order to use this command , you must be the HG winner !");
			return 0;
		}
		char[] info = new char[16];
		menu.GetItem(item, info, 16);
		
		hgtype = view_as<HGTypes>(StringToInt(info));
		
		if(hgtype == HG_Normal)
		{
			ShowWeaponsMenu(client);
			return 0;
		}
		
		if(hgtype == HG_JumpShots)
		{
			ShowWeaponsMenu(client);
			return 0;
		}
		
		CPrintToChatAll("%s The winner have choosed the mode : \x07%s", PREFIX, HGTypesNames[view_as<int>(hgtype)]);
		CS_TerminateRound(3.0, CSRoundEnd_Draw);
		
	}
	if(action == MenuAction_Cancel)
	{
		delete menu;
	}
	if(action == MenuAction_End)
	{
		delete menu;
	}
	return 0;
}


public Action Cmd_Winner(int client, int args)
{
	if(!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Handled;
	}
	if(!IsPlayerAlive(client))
	{
		CPrintToChat(client, "%s In order to use this command , you must be alive !");
		return Plugin_Handled;
	}
	if(GetAliveT() != 1)
	{
		CPrintToChat(client, "%s In order to use this command , there must an HG winner !");
		return Plugin_Handled;
	}
	if(client != gB_HGPlayers[HGWinner])
	{
		CPrintToChat(client, "%s In order to use this command , you must be the HG winner !");
		return Plugin_Handled;
	}
	
	Menu menu = new Menu(WinnerMenuHandler);
	menu.SetTitle("Choose GameMode for the HG");
	for(int i = 1; i < sizeof(HGTypesNames); i++)
	{
		char[] sInfo = new char[8];
		IntToString(i, sInfo, 8);
		
		menu.AddItem(sInfo, HGTypesNames[i]);
	}
	
	// just to be sure ;-;
	if(menu.ItemCount == 0)
	{
		menu.AddItem("-1", "Nothing to see here");
	}
	menu.ExitButton = false;
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}

public int WeaponMenuHandler(Menu menu, MenuAction action, int client, int item)
{
	if(action == MenuAction_Select)
	{
    	char strInfo[32];
    	menu.GetItem(item, strInfo, sizeof(strInfo));
    	
    	Menu menu2 = new Menu(MenuWeaponHandler);
        char strTitle[32];
        Format(strTitle, sizeof(strTitle), "%s", WeaponsCategMenu[item]);
        menu2.SetTitle(strTitle);
        for(int a = 0; a < sizeof(WeaponsNameForMenu[]); a++)
        {
        	if(WeaponsNameForMenu[item][a][0] == '0')
        		break;
        	
        	menu2.AddItem(WeaponsAliasForMenu[item][a], WeaponsNameForMenu[item][a]);
        }
        
        menu2.ExitBackButton = true;
        menu2.ExitButton = false;
        menu2.Display(client, MENU_TIME_FOREVER);
    }
	if(action == MenuAction_Cancel)
	{
		delete menu;
	}
	if(action == MenuAction_End)
	{
		delete menu;
	}
}

public int MenuWeaponHandler(Menu menu2, MenuAction action, int client, int item)
{
	if(action == MenuAction_Select)
	{
		menu2.GetItem(item, HGWeapon, sizeof(HGWeapon));
		CPrintToChatAll("%s The winner choose : \x07%s", PREFIX, HGWeapon);
		CS_TerminateRound(3.0, CSRoundEnd_Draw);
		CPrintToChatAll("%s The winner have choosed the mode : \x07%s", PREFIX, HGTypesNames[view_as<int>(hgtype)]);
		CPrintToChatAll("%s With the weapon : \x07%s", PREFIX, HGWeapon);
	}
	if(action == MenuAction_Cancel)
	{
		delete menu2;
	}
	if(action == MenuAction_End)
	{
		delete menu2;
	}
}

void ShowWeaponsMenu(int client)
{
	if(!IsValidClient(client))
	{
		return;
	}
	if(!gB_HGEnabled.IntValue)
	{
		return;
	}
	if(!IsPlayerAlive(client))
	{
		CPrintToChat(client, "%s In order to use this command , you must be alive !");
		return;
	}
	if(GetAliveT() != 1)
	{
		CPrintToChat(client, "%s In order to use this command , there must an HG winner !");
		return;
	}
	if(client != gB_HGPlayers[HGWinner])
	{
		CPrintToChat(client, "%s In order to use this command , you must be the HG winner !");
		return;
	}
	
	Menu menu = new Menu(WeaponMenuHandler);
	menu.SetTitle("Choose weapons for the HG");
	for(int a = 0; a < sizeof(WeaponsCategMenu); a++)
	{
		menu.AddItem("", WeaponsCategMenu[a]);
	}
	menu.ExitButton = false;
	menu.Display(client, MENU_TIME_FOREVER);
}

// ==================================================================================================================================================== //

public int HandlerMenu(Menu menu, MenuAction action, int client, int item)
{
	if(action == MenuAction_Select)
	{
		delete menu;
	}
	if(action == MenuAction_Cancel)
	{
		delete menu;
	}
}

public Action Cmd_Most(int client, int args)
{
	if(!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Handled;
	}
	
	int mostkills = 0;
	
	Menu menu = new Menu(HandlerMenu);
	menu.SetTitle("Most kills player : ");
	
	int playername;
	for(int i = 1; i < MaxClients; i++)
	{
		if(IsClientInGame(i) && PlayerKills[i] > 0)
		{
			if(PlayerKills[i] > mostkills || PlayerKills[i] == mostkills)
			{
				mostkills = PlayerKills[i];
				playername = i;
			}

		}
	}
	char Shit[128];
	char PlayerNames[MAX_NAME_LENGTH];
	GetClientName(playername, PlayerNames, MAX_NAME_LENGTH);
	Format(Shit, 128, "%s - %d", PlayerNames, mostkills);
	
	
	menu.AddItem("", Shit);
	
	
	// just to be sure ;-;
	if(menu.ItemCount == 0)
	{
		menu.AddItem("-1", "Nothing to see here");
	}
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	
	return Plugin_Handled;
}

public Action Cmd_RestartMost(int client, int args)
{
	if(!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Handled;
	}
	
	for (int i = 0; i <= MaxClients; i++)
	{
		if(IsValidClient(i))
		{
			PlayerKills[i] = 0;
		}
	}	
	
	CPrintToChat(client, "%s You cleaned all the kills.", PREFIX);
	return Plugin_Handled;
}

// ==================================================================================================================================================== //

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3])
{
	if(!gB_HGEnabled.IntValue)
	{
		return;
	}
	if(!IsValidClient(client))
	{
		return;
	}
	if(buttons & IN_USE)
	{
		int entidad = GetClientAimTarget(client);
		if(entidad > 0)
		{
			if(UsingTeam[entidad] != 0) return;

			if(GetClientAimTarget(entidad) != client) return;

			float OriginG[3], TargetOriginG[3];
			GetClientEyePosition(client, TargetOriginG);
			GetClientEyePosition(entidad, OriginG);
			if(GetVectorDistance(TargetOriginG,OriginG, false) > 200.0) return;

			PrintCenterText(client, "Sending team request to %N", entidad);
			PrintCenterText(entidad, "Player %N are sending a team request to you.\nFor accept just press +USE (E button).", client);
			if(!(GetClientButtons(entidad) & IN_USE)) return;

			SendRequest(client, entidad);
		}
	}
}


void SendRequest(int client, int target)
{
        int Random1 = GetRandomInt(1, 255);
        int Random2 = GetRandomInt(1, 255);
        int Random3 = GetRandomInt(1, 255);
        
        if(UsingTeam[client] == 0 && UsingTeam[target] == 0)
        {
        	UsingTeam[client] = 1;
        	UsingTeam[target] = 1;
        	
        	
        	SetEntityRenderColor(client, Random1, Random2, Random3);
        	SetEntityRenderColor(target, Random1, Random2, Random3);
        	
        	AlliedWith[client] = target;
        	AlliedWith[target] = client;
        	
        	PrintHintText(client, "Team request accepted by %N.", target);
        	PrintHintText(target, "Team request accepted by %N.", client);
        	CPrintToChat(client, "%s You can delete your team by using : /delteam", PREFIX);
        	CPrintToChat(target, "%s You can delete your team by using : /delteam", PREFIX);
        }
}

// ==================================================================================================================================================== //

public Action Cmd_DelTeam(int client, int args)
{
	if(!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	if(!gB_HGEnabled.IntValue)
	{
		return Plugin_Handled;
	}
	
	if(UsingTeam[client] == 0)
	{
		CPrintToChat(client, "%s You are not currently in a team", PREFIX);
	}
	else
	{
		int target = AlliedWith[client];
		
		CPrintToChat(client, "%s Your team has been disbanded !", PREFIX);
		CPrintToChat(target, "%s Your team has been disbanded !", PREFIX);
		
		
		AlliedWith[target] = 0;
		AlliedWith[client] = 0;
		
		UsingTeam[target] = 0;
		UsingTeam[client] = 0;
		
		
		SetEntityRenderMode(client, RENDER_NORMAL);
		SetEntityRenderColor(client, 255, 255, 255);
		SetEntityRenderMode(target, RENDER_NORMAL);
		SetEntityRenderColor(target, 255, 255, 255);
	}
	return Plugin_Handled;
}

// ==================================================================================================================================================== //

public void OnEntityCreated(int entity, const char[] classname)
{
	if(StrEqual(classname, "flashbang_projectile") && hgtype == HG_Dodgeball)
	{
		SDKHook(entity, SDKHook_Spawn, OnEntitySpawned);
	}
}

public void OnEntitySpawned(int entity)
{
	CreateTimer(0.0, Timer_RemoveThinkTick, entity, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_RemoveThinkTick(Handle timer, any entity)
{
	SetEntProp(entity, Prop_Data, "m_nNextThinkTick", -1);
	CreateTimer(1.4, Timer_RemoveFlashbang, entity, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_RemoveFlashbang(Handle timer, any entity)
{
	if (IsValidEntity(entity))
	{
		int client = GetEntPropEnt(entity, Prop_Data, "m_hOwnerEntity");
		AcceptEntityInput(entity, "Kill");
			
		if ((client != -1) && IsClientInGame(client) && IsPlayerAlive(client))
		{
			GivePlayerItem(client, "weapon_flashbang");
			FakeClientCommand(client, "use weapon_flashbang");
		}
	}
}

// ==================================================================================================================================================== //