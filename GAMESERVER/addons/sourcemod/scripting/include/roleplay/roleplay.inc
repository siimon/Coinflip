#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <smlib>
#include <multicolors>

#include "roleplay/roleplay_buildings"
#include "roleplay/roleplay_building_bank_access"
#include "roleplay/roleplay_building_appartmenthouse"
#include "roleplay/roleplay_building_appartmenthouse_access"
#include "roleplay/roleplay_building_hospital_access"
#include "roleplay/roleplay_building_trainingcenter_access"
#include "roleplay/roleplay_building_shop_access"
#include "roleplay/roleplay_building_club_access"
#include "roleplay/roleplay_building_gangbase_access"
#include "roleplay/roleplay_building_homehouse_access"
#include "roleplay/roleplay_building_policedepartment_access"
#include "roleplay/roleplay_clash"
#include "roleplay/roleplay_crime"
#include "roleplay/roleplay_csgo"
#include "roleplay/roleplay_doors"
#include "roleplay/roleplay_furniture_operations"
#include "roleplay/roleplay_items"
#include "roleplay/roleplay_items_ammo"
#include "roleplay/roleplay_items_furniture"
#include "roleplay/roleplay_items_medic"
#include "roleplay/roleplay_items_weapon"
#include "roleplay/roleplay_jail"
#include "roleplay/roleplay_level"
#include "roleplay/roleplay_money"
#include "roleplay/roleplay_npcs"
#include "roleplay/roleplay_players"
#include "roleplay/roleplay_sql"
#include "roleplay/roleplay_teams"
#include "roleplay/roleplay_vendors"
#include "roleplay/roleplay_weapon"

/* CORE */
#define PL_VERSION		"0.0.1 alpha"
#define PL_AUTHOR		"Gate; Zipcore"
#define PL_URL			"fotg.net"
#define PL_PRE 			"{lightred}[{green}RP{lightred}]{green}"
#define PL_PRE_N 		"[RP]"
#define PL_PRE_T 		"{lightred}[{green}RP{lightred}]{green} %t"

/* CSTRIKE */
#define CS_TEAMS_NUM 4
#define CS_SLOT_KNIFE 2
#define ITEMS_TYPES_NUM 10
#define ITEMS_STR_LEN 32
#define MAX_CASH 16000
#define DEFAULT_MAX_ARMOR 100
#define CS_MAX_ARMOR 127

/* SteamID */
stock bool:SteamID_GetCommunityID(String:AuthID[], String:CommunityID[], size)
{
	if(strlen(AuthID) < 11 || AuthID[0]!='S' || AuthID[6]=='I')
	{
		FriendID[0] = 0;
		return false;
	}
	new iUpper = 765611979;
	new CommunityID = StringToInt(AuthID[10])*2 + 60265728 + AuthID[8]-48;
	new iDiv = CommunityID/100000000;
	new iIdx = 9-(iDiv?iDiv/10+1?0);
	iUpper += iDiv;
	IntToString(CommunityID, CommunityID[iIdx], size-iIdx);
	iIdx = FriendID[9];
	IntToString(iUpper, CommunityID, size);
	FriendID[9] = iIdx;
	return true;
}

stock FindPlayer(client, String:idstr[])
{
	new uid = StringToInt(idstr);
	new id = GetClientOfUserId(uid);
	new MaxC = GetMaxClients();
	
	if(id == 0)
	{
		new numclients = 0;
		new numid = 0;
		new String:name[50];
		
		for(new i=1;i<MaxC;i++)
		{
			if(!IsClientInGame(i)) continue;
			GetClientName(i, name, 50);
			if(StrContains(name, idstr, false) != -1)
			{
				numclients++;
				if(numclients > 1) break;
				numid = i;
			}
		}
		
		if(numclients > 1)
		{
			CPrintToChat(client,"There is more than one client matching that string, please be more specific.");
			return 0;
		}
		
		id = numid;
		
		if(id == 0)
			return 0;
	}
	return id;
}

stock SteamID_GetClient(String:steamid[])
{
	new String:authid[32];
	for(new i=1;i<=MaxClients;++i)
	{
		if(!IsClientInGame(i))
			continue;
		if(!IsClientAuthorized(i))
			continue;
		GetClientAuthString(i, authid, sizeof(authid));
		if(strcmp(authid, steamid)==0)
			return i;
	}
	return 0;
}

/* Teams */
stock Team_GetPlayerCount(iTeam, bool:alive=true)
{
	new iCount = 0;
	for(new client = 1; client <= MaxClients; client++)
	{
		if(!IsClientInGame(client))
			continue;
		if(GetClientTeam(client) != iTeam)
			continue;
		if(!IsPlayerAlive(client) && alive)
			continue;
		
		iCount++;
	}
	return iCount;
}

/* Client */

stock bool:IsPlayer(client)
{
	return (client > 0 && client <= MAXPLAYERS);
}

stock FindMatchingPlayers(const String:matchstr[], clients[])
{
    new count=0;
    if(StrEqual(matchstr,"@all",false))
    {
        for(new x=1;x<=MaxClients;x++)
        {
            if(IsClientInGame(x))
            {
                clients[count]=x;
                count++;
            }
        }
    }
    else if(StrEqual(matchstr,"@t",false))
    {
        for(new x=1;x<=MaxClients;x++)
        {
            if(IsClientInGame(x)&&GetClientTeam(x)==CSS_TEAM_T)
            {
                clients[count]=x;
                count++;
            }
        }
    }
    else if(StrEqual(matchstr,"@ct",false))
    {
        for(new x=1;x<=MaxClients;x++)
        {
            if(IsClientInGame(x)&&GetClientTeam(x)==CSS_TEAM_CT)
            {
                clients[count]=x;
                count++;
            }
        }
    }
    else if(matchstr[0]=='@')
    {
        new userid=StringToInt(matchstr[1]);
        if(userid)
        {
            new index=GetClientOfUserId(userid);
            if(index)
            {
                if(IsClientInGame(index))
                {
                    clients[count]=index;
                    count++;
                }
            }
        }
    }
    else
    {
        decl String:name[64];
        for(new x=1;x<=MaxClients;x++)
        {
            if(IsClientInGame(x))
            {
                GetClientName(x,name,sizeof(name));
                if(StrContains(name,matchstr,false)!=-1)
                {
                    clients[count]=x;
                    count++;
                }
            }
        }
    }
    return count;
}

stock Client_StripWeapons(client)
{
	new iWeapon = -1;
	for(new i=CS_SLOT_PRIMARY;i<=CS_SLOT_C4;i++)
		while((iWeapon = GetPlayerWeaponSlot(client, i)) != -1)
			CS_DropWeapon(client, iWeapon, true, true);
}

stock Client_RemoveWeapons(client)
{
	new iWeapon = -1;
	for(new i=CS_SLOT_PRIMARY;i<=CS_SLOT_C4;i++)
		while((iWeapon = GetPlayerWeaponSlot(client, i)) != -1)
			AcceptEntityInput(iWeapon, "kill");
}

stock bool:Client_RemoveWeaponBySlot(client, iSlot)
{
	new iEntity = GetPlayerWeaponSlot(client, iSlot);
	if(IsValidEdict(iEntity))
	{
		RemovePlayerItem(client, iEntity);
		AcceptEntityInput(iEntity, "Kill");
		return true;
	}
	return false;
}

stock Client_GetPrimaryAmmo(client) 
{   
	new weapon = GetPlayerWeaponSlot(client, 0);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType)) + GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock Client_GetSecondaryAmmo(client)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType)) + GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock Client_GetPrimaryAmmoClip(client) 
{   
	new weapon = GetPlayerWeaponSlot(client, 0);
	return GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock Client_GetSecondaryAmmoClip(client)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	return GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock Client_GetPrimaryAmmoHolster(client) 
{   
	new weapon = GetPlayerWeaponSlot(client, 0);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType));
}

stock Client_GetSecondaryAmmoHolster(client)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType));
}

stock Client_SetPrimaryAmmoClip(client, value)
{
	new weapon = GetPlayerWeaponSlot(client, 0);
	SetEntProp(weapon, Prop_Data, "m_iClip1", value);
}

stock Client_SetSecondaryAmmoClip(client, value)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	SetEntProp(weapon, Prop_Data, "m_iClip1", value);
}

stock Client_SetPrimaryAmmoHolster(client, value)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	new weapon = GetPlayerWeaponSlot(client, 0);	
	SetEntData(client, FindSendPropOffs("CBasePlayer", "m_iAmmo")+(GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType")*4), value, 4, true);
}

stock Client_SetSecondaryAmmoHolster(client, value)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	new weapon = GetPlayerWeaponSlot(client, 1);	
	SetEntData(client, FindSendPropOffs("CBasePlayer", "m_iAmmo")+(GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType")*4), value, 4, true);
}

stock Client_SetHelmet(client, status)
{
	SetEntProp( client, Prop_Send, "m_bHasHelmet", status);
}

stock Client_SetSpeed(client, Float:fspeed)
{
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", fspeed);	
}

stock Client_SetNightvision(client, enable)
{
	SetEntProp(client, Prop_Send, "m_bNightVisionOn", enable);
}

stock Client_ToggleFlashlight(client)
{
	SetEntProp(client, Prop_Send, "m_fEffects", GetEntProp(client, Prop_Send, "m_fEffects") ^ 4);
}

stock Client_SetFlashMaxAlpha(client)
{
	SetEntPropFloat(client, Prop_Send, "m_flFlashMaxAlpha", 0.5);
}

/* Entity */
public Action:Entity_Delete(Handle:timer, any:entity)
{
	if (IsValidEntity(entity)) 
		AcceptEntityInput(entity, "kill");
}

stock SetCollidable(client, bool:collidable)
{
	SetEntData(entity, FindSendPropOffs("CBaseEntity", "m_CollisionGroup"), collidable ? 5 : 2, 4, true);
}

stock ExtinguishClient(client)
{
	new ent = GetEntPropEnt(client, Prop_Data, "m_hEffectEntity");
	if (IsValidEdict(ent))
	{
		decl String:classname[64];
		GetEdictClassname(ent, classname, sizeof(classname));
		if (StrEqual(classname, "entityflame", false))
			SetEntPropFloat(ent, Prop_Data, "m_flLifetime", 0.0);
	}
}

#define FFADE_IN 		0x0001        // Just here so we don't pass 0 into the function
#define FFADE_OUT		0x0002        // Fade out (not in)
#define FFADE_MODULATE	0x0004        // Modulate (don't blend)
#define FFADE_STAYOUT	0x0008        // ignores the duration, stays faded out until new ScreenFade message received
#define FFADE_PURGE		0x0010        // Purges all other fades, replacing them with this one

stock Client_PerformBlind(client, amountm, FFADE_flags ,duration = 1536, holdtime = 1536)
{
	new targets[2];
	targets[0] = client;
	
	new color[4] = { 0, 0, 0, 0 };
	color[3] = amount;
	
	new Handle:message = StartMessageEx(g_FadeUserMsgId, targets, 1);
	if (GetUserMessageType() == UM_Protobuf)
	{
		PbSetInt(message, "duration", duration);
		PbSetInt(message, "hold_time", holdtime);
		PbSetInt(message, "flags", flags);
		PbSetColor(message, "clr", color);
	}
	else
	{
		BfWriteShort(message, duration);
		BfWriteShort(message, holdtime);
		BfWriteShort(message, flags);		
		BfWriteByte(message, color[0]);
		BfWriteByte(message, color[1]);
		BfWriteByte(message, color[2]);
		BfWriteByte(message, color[3]);
	}
	
	EndMessage();
}

/* Timer */
stock ClearTimer(&Handle:timer)
{
	if(timer != INVALID_HANDLE)
	{
		KillTimer(timer);
		timer = INVALID_HANDLE;
	}
}

/* Menu */
stock bool:AddMenuItemEx(Handle:menu, style, String:info[], String:display[], any:...)
{
	decl String:m_display[256];
	VFormat(m_display, sizeof(m_display), display, 5);
	return (AddMenuItem(menu, info, m_display, style)?true:false);
}

stock bool:InsertMenuItemEx(Handle:menu, position, style, String:info[], String:display[], any:...)
{
	decl String:m_display[256];
	VFormat(m_display, sizeof(m_display), display, 6);
	if(GetMenuItemCount(menu)==position)
		return (AddMenuItem(menu, info, m_display, style)?true:false);
	else
		return (InsertMenuItem(menu, position, info, m_display, style)?true:false);
}

stock AutoPaginate(Handle:menu)
{
	if (menu == INVALID_HANDLE)
	{
		return;
	}
	
	new itemsPerPage = GetMaxPageItems(GetMenuStyle(menu));
	
	new bool:pageOneHasExtraItem = false;
	
	if (GetMenuExitButton(menu))
		itemsPerPage--;
	
	if (GetMenuOptionFlags(menu) | MENUFLAG_BUTTON_NOVOTE)
		pageOneHasExtraItem = true;
	
	new count = GetMenuItemCount(menu);
	
	if (count <= (pageOneHasExtraItem ? itemsPerPage - 1 : itemsPerPage ))
		SetMenuPagination(menu, MENU_NO_PAGINATION);
	else SetMenuPagination(menu, itemsPerPage - 2);
}

/* Time Format*/
stock SecondsToString(String:sBuffer[], iLength, iSecs, bool:bTextual = true)
{
	if(bTextual)
	{
		decl String:sDesc[6][8] = {"mo", 				"wk", 				"d", 			"hr", 		"min", 	"sec"};
		new iCount, 	iDiv[6] = {60 * 60 * 24 * 30, 	60 * 60 * 24 * 7, 	60 * 60 * 24, 	60 * 60, 	60, 	1};
		sBuffer[0] = '\0';
		
		for(new i = 0; i < sizeof(iDiv); i++)
		{
			if((iCount = iSecs / iDiv[i]) > 0)
			{
				Format(sBuffer, iLength, "%s%i %s, ", sBuffer, iCount, sDesc[i]);
				iSecs %= iDiv[i];
			}
		}
		sBuffer[strlen(sBuffer) - 2] = '\0';
	}
	else
	{
		new iHours = iSecs / 60 / 60;
		iSecs -= iHours * 60 * 60;
		new iMins = iSecs / 60;
		iSecs %= 60;
		Format(sBuffer, iLength, "%02i:%02i:%02i", iHours, iMins, iSecs);
	}
}

/* Effects */
stock env_shooter(Float:Angles[3], Float:iGibs, Float:Delay, Float:GibAngles[3], Float:Velocity, Float:Variance, Float:Giblife, Float:Location[3], String:ModelType[])
{
	//decl Ent;

	//Initialize:
	new Ent = CreateEntityByName("env_shooter");
		
	//Spawn:

	if (Ent == -1)
		return;

  	//if (Ent>0 && IsValidEdict(Ent))

	if(Ent>0 && IsValidEntity(Ent) && IsValidEdict(Ent))
  	{

		//Properties:
		//DispatchKeyValue(Ent, "targetname", "flare");

		// Gib Direction (Pitch Yaw Roll) - The direction the gibs will fly. 
		DispatchKeyValueVector(Ent, "angles", Angles);
	
		// Number of Gibs - Total number of gibs to shoot each time it's activated
		DispatchKeyValueFloat(Ent, "m_iGibs", iGibs);

		// Delay between shots - Delay (in seconds) between shooting each gib. If 0, all gibs shoot at once.
		DispatchKeyValueFloat(Ent, "delay", Delay);

		// <angles> Gib Angles (Pitch Yaw Roll) - The orientation of the spawned gibs. 
		DispatchKeyValueVector(Ent, "gibangles", GibAngles);

		// Gib Velocity - Speed of the fired gibs. 
		DispatchKeyValueFloat(Ent, "m_flVelocity", Velocity);

		// Course Variance - How much variance in the direction gibs are fired. 
		DispatchKeyValueFloat(Ent, "m_flVariance", Variance);

		// Gib Life - Time in seconds for gibs to live +/- 5%. 
		DispatchKeyValueFloat(Ent, "m_flGibLife", Giblife);
		
		// <choices> Used to set a non-standard rendering mode on this entity. See also 'FX Amount' and 'FX Color'. 
		DispatchKeyValue(Ent, "rendermode", "5");

		// Model - Thing to shoot out. Can be a .mdl (model) or a .vmt (material/sprite). 
		DispatchKeyValue(Ent, "shootmodel", ModelType);

		// <choices> Material Sound
		DispatchKeyValue(Ent, "shootsounds", "-1"); // No sound

		// <choices> Simulate, no idea what it realy does tbh...
		// could find out but to lazy and not worth it...
		//DispatchKeyValue(Ent, "simulation", "1");

		SetVariantString("spawnflags 4");
		AcceptEntityInput(Ent,"AddOutput");

		ActivateEntity(Ent);

		//Input:
		// Shoot!
		AcceptEntityInput(Ent, "Shoot", 0);
			
		//Send:
		TeleportEntity(Ent, Location, NULL_VECTOR, NULL_VECTOR);

		//Delete:
		//AcceptEntityInput(Ent, "kill");
		RemoveEntity(Ent, 1.0);
	}
}

stock bool:env_explosion(attacker = 0, inflictor = -1, const Float:attackposition[3], const String:weaponname[] = "", magnitude = 100, radiusoverride = 0, Float:damageforce = 0.0, flags = 0)
{
	
	new explosion = CreateEntityByName("env_explosion");
	
	if(explosion != -1)
	{
		DispatchKeyValueVector(explosion, "Origin", attackposition);
		
		decl String:intbuffer[64];
		IntToString(magnitude, intbuffer, 64);
		DispatchKeyValue(explosion,"iMagnitude", intbuffer);
		if(radiusoverride > 0)
		{
			IntToString(radiusoverride, intbuffer, 64);
			DispatchKeyValue(explosion,"iRadiusOverride", intbuffer);
		}
		
		if(damageforce > 0.0)
			DispatchKeyValueFloat(explosion,"DamageForce", damageforce);

		if(flags != 0)
		{
			IntToString(flags, intbuffer, 64);
			DispatchKeyValue(explosion,"spawnflags", intbuffer);
		}

		if(!StrEqual(weaponname, "", false))
			DispatchKeyValue(explosion,"classname", weaponname);

		DispatchSpawn(explosion);
		if(attacker > 0 && Client_IsValid(attacker, true) &&  IsClientInGame(attacker))
			SetEntPropEnt(explosion, Prop_Send, "m_hOwnerEntity", attacker);

		if(inflictor != -1)
			SetEntPropEnt(explosion, Prop_Data, "m_hInflictor", inflictor);
			
		AcceptEntityInput(explosion, "Explode");
		AcceptEntityInput(explosion, "Kill");
		
		return (true);
	}
	else
		return (false);
}

stock env_shake(Float:Origin[3], Float:Amplitude, Float:Radius, Float:Duration, Float:Frequency)
{
	decl Ent;

	//Initialize:
	Ent = CreateEntityByName("env_shake");
		
	//Spawn:
	if(DispatchSpawn(Ent))
	{
		//Properties:
		DispatchKeyValueFloat(Ent, "amplitude", Amplitude);
		DispatchKeyValueFloat(Ent, "radius", Radius);
		DispatchKeyValueFloat(Ent, "duration", Duration);
		DispatchKeyValueFloat(Ent, "frequency", Frequency);

		SetVariantString("spawnflags 8");
		AcceptEntityInput(Ent,"AddOutput");

		//Input:
		AcceptEntityInput(Ent, "StartShake", 0);
		
		//Send:
		TeleportEntity(Ent, Origin, NULL_VECTOR, NULL_VECTOR);

		//Delete:
		RemoveEntity(Ent, 30.0);
	}
}

stock GetEntityName(entity, String:name[],maxlen)
{
	GetEntPropString(entity, Prop_Data, "m_iName", name, maxlen);
}