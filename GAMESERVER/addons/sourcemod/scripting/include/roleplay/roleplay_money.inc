#if defined _rp_money_included
 #endinput
#endif
#define _rp_money_included

native RP_GetMoney(client);
native RP_SetMoney(client, amount);
native RP_AddMoney(client, amount);
native RP_TakeMoney(client, amount);

native RP_GetMoneyBank(client);
native RP_SetMoneyBank(client, amount);
native RP_AddMoneyBank(client, amount);
native RP_TakeMoneyBank(client, amount);

native bool:RP_TransferMoneyCash(client, target, amount);
native bool:RP_TransferMoneyBank(client, targetID, amount);


public __pl_rp_money_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetMoney");
	MarkNativeAsOptional("RP_SetMoney");
	MarkNativeAsOptional("RP_AddMoney");
	MarkNativeAsOptional("RP_TakeMoney");
	
	MarkNativeAsOptional("RP_GetMoneyBank");
	MarkNativeAsOptional("RP_SetMoneyBank");
	MarkNativeAsOptional("RP_AddMoneyBank");
	MarkNativeAsOptional("RP_TakeMoneyBank");
	
	MarkNativeAsOptional("RP_TransferMoneyCash");
	MarkNativeAsOptional("RP_TransferMoneyBank");
}