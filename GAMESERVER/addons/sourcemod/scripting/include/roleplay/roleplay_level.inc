#if defined _rp_level_included
 #endinput
#endif
#define _rp_level_included

stock Float:RP_GetPlayerLevelRatio(victim, attacker)
{
    new vl = RP_GetLevel(victim);
    new al = RP_GetLevel(attacker);
    new Float:ratio;
    
    if(al > 1)
        ratio = float(vl)/float(al);
    else ratio = float(vl);

    if(ratio <= 0.0)
        ratio = 1.0;

    return ratio;
}

forward RP_OnPlayerLevelUp(client, level);
	
native RP_GetExp(client);
native RP_AddExp(client, amount);
native RP_SetExp(client, amount);
	
native RP_GetLevel(client);
native RP_AddLevel(client, amount);
native RP_SetLevel(client, amount);

public __pl_rp_level_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetExp");
	MarkNativeAsOptional("RP_AddExp");
	MarkNativeAsOptional("RP_SetExp");
	
	MarkNativeAsOptional("RP_GetLevel");
	MarkNativeAsOptional("RP_AddLevel");
	MarkNativeAsOptional("RP_SetLevel");
}