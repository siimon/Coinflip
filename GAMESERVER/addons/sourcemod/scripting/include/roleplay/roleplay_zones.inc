#if defined _rp_zones_included
 #endinput
#endif
#define _rp_zones_included

forward RP_OnAllZonesLoaded();

forward RP_OnPlayerStartTouchZone(client, entity, zoneIndex, type, subType);
forward RP_OnPlayerEndTouchZone(client, entity, zoneIndex, type, subType);

/*
native RP_GetZoneCount();
native RP_GetZoneCountForType(type);
native RP_GetZoneCountForSubType(type, subType);

native RP_IsEntityTouchingZoneType(entity, type);
native RP_IsEntityTouchingZoneSubType(entity, type, subType);

native RP_GetZoneIndexByEnity(entity);
native RP_GetZoneIndexById(zoneId);
native RP_GetZoneIdByEnity(entity);
native RP_GetZoneIdByIndex(zoneIndex);
native RP_GetZoneEnityById(zoneId);
native RP_GetZoneEnityByIndex(zoneIndex);
*/

native RP_GetZoneType(zoneIndex);
native RP_GetZoneSubType(zoneIndex);
native RP_GetZonePoints(zoneIndex, Float:&point1[3], Float:&point2[3], Float:&center[3]);
native RP_GetZoneName(zoneIndex, String:buffer[], length_32);
native RP_GetZoneSettings(zoneIndex, String:buffer[], length_512);

native bool:RP_IsPosInsideZone(zoneIndex, Float:pos[3]);

public __pl_rp_zones_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_IsPlayerTouchingZoneType");
	MarkNativeAsOptional("RP_IsPlayerTouchingZoneSubType");
	MarkNativeAsOptional("RP_IsEntityTouchingZoneType");
	MarkNativeAsOptional("RP_IsEntityTouchingZoneSubType");
	
	MarkNativeAsOptional("RP_GetZoneIndexByEnity");
	MarkNativeAsOptional("RP_GetZoneIndexById");
	MarkNativeAsOptional("RP_GetZoneIdByEnity");
	MarkNativeAsOptional("RP_GetZoneIdByIndex");
	MarkNativeAsOptional("RP_GetZoneEnityById");
	MarkNativeAsOptional("RP_GetZoneEnityByIndex");
	
	MarkNativeAsOptional("RP_GetZoneType");
	MarkNativeAsOptional("RP_GetZoneSubType");
	MarkNativeAsOptional("RP_GetZonePoints");
	MarkNativeAsOptional("RP_GetZoneSettings");
	
	MarkNativeAsOptional("RP_IsPosInsideZone");
}