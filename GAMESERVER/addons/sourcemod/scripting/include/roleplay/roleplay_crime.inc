#if defined _rp_crime_included
 #endinput
#endif
#define _rp_crime_included

native RP_GetCrime(client);
native RP_AddCrime(client, amount, String:sType[]);
native RP_TakeCrime(client, amount);
native RP_ResetCrime(client);

native RP_GetLastCrime(client);
native RP_GetLastCrimeRemoval(client);

native RP_GetBounty(client);
native RP_ResetBounty(client);
native RP_AddBounty(client, amount);
native RP_TakeBounty(client, amount);

native RP_GetMaxCrime(client);
native RP_GetMaxBounty(client);

public __pl_rp_crime_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetCrime");
	MarkNativeAsOptional("RP_SetCrime");
	MarkNativeAsOptional("RP_AddCrime");
	MarkNativeAsOptional("RP_TakeCrime");
	
	MarkNativeAsOptional("RP_GetLastCrime");
	MarkNativeAsOptional("RP_GetLastCrimeRemoval");
	
	MarkNativeAsOptional("RP_GetBounty");
	MarkNativeAsOptional("RP_ResetBounty");
	MarkNativeAsOptional("RP_AddBounty");
	MarkNativeAsOptional("RP_TakeBounty");
	
	MarkNativeAsOptional("RP_GetMaxCrime");
	MarkNativeAsOptional("RP_GetMaxBounty");
}