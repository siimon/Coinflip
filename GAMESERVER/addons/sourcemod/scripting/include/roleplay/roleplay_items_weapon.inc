#if defined _rp_items_weapon_included
 #endinput
#endif
#define _rp_items_weapon_included

native bool:RP_GetWeaponSlotItem(client, WeaponSlot);
native bool:RP_SetWeaponSlotItem(client, WeaponSlot, itemID);
native bool:RP_GetWeaponSlotEntity(client, WeaponSlot);

public __pl_rp_items_weapon_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetWeaponSlotItem");
	MarkNativeAsOptional("RP_SetWeaponSlotItem");
	MarkNativeAsOptional("RP_GetWeaponSlotEntity");
}