#if defined _rp_building_appartmenthouse_doors_included
 #endinput
#endif
#define _rp_building_appartmenthouse_doors_included

enum RP_Appartments
{
	Appartment_ID,
	bool:Appartment_Sold,
	Appartment_PlayerID,
	Appartment_Price,
	Appartment_MaxInh,
	Building_ID,
	Appartment_Roomnumber
}

native RP_GetAppartmentCount();

native RP_GetAppartmentIDByIndex(index);
native RP_GetAppartmentIDByPlayerID(playerID);

native RP_CreateAppartment(String:Appartmentname[], price, instance_start, instance_end);
native RP_DeleteAppartment(AppartmentID);

native RP_GetAppartmentPlayerID(AppartmentID);
native RP_SetAppartmentPlayerID(AppartmentID, playerID);

native RP_GetAppartmentPrice(AppartmentID);
native RP_SetAppartmentPrice(AppartmentID, price);

native bool:RP_IsAppartmentold(AppartmentID);
native RP_SetAppartmentold(AppartmentID, bool:isSold);

native RP_GetAppartmentMaxInh(AppartmentID);
native RP_SetAppartmentMaxInh(AppartmentID,new_max);

native RP_GetAppartmentbuildingID(AppartmentID);
native RP_SetAppartmentbuildingID(AppartmentID,new_buildingID);

native RP_GetAppartmentRoomnumber(AppartmentID);
native RP_SetAppartmentRoomnumber(AppartmentID,new_Roomnumber);

public __pl_rp_building_appartmenthouse_doors_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetAppartmentCount");
	
	MarkNativeAsOptional("RP_CreateAppartment");
	MarkNativeAsOptional("RP_DeleteAppartment");
	
	MarkNativeAsOptional("RP_SetAppartmentPlayerID");
	MarkNativeAsOptional("RP_GetAppartmentPlayerID");
	
	MarkNativeAsOptional("RP_SetAppartmentPrice");
	MarkNativeAsOptional("RP_GetAppartmentPrice");
	
	MarkNativeAsOptional("RP_SetAppartmentSold");
	MarkNativeAsOptional("RP_IsAppartmentSold");
	
	MarkNativeAsOptional("RP_GetAppartmentIDByIndex");
	MarkNativeAsOptional("RP_GetAppartmentIDByPlayerID");
	
	MarkNativeAsOptional("RP_GetAppartmentMaxInh");
	MarkNativeAsOptional("RP_SetAppartmentMaxInh");
	
	MarkNativeAsOptional("RP_GetAppartmentbuildingID");
	MarkNativeAsOptional("RP_SetAppartmentbuildingID");
	
	MarkNativeAsOptional("RP_GetAppartmentRoomnumber");
	MarkNativeAsOptional("RP_SetAppartmentRoomnumber");
}