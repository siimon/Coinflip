/** Double-include prevention */
#if defined _rp_csgo_included_
  #endinput
#endif
#define _rp_csgo_included_

stock GetPrimaryAmmo(client) 
{   
	new weapon = GetPlayerWeaponSlot(client, 0);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType)) + GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock GetSecondaryAmmo(client)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType)) + GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock GetPrimaryAmmoClip(client) 
{   
	new weapon = GetPlayerWeaponSlot(client, 0);
	return GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock GetSecondaryAmmoClip(client)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	return GetEntProp(weapon, Prop_Data, "m_iClip1");
}

stock GetPrimaryAmmoHolster(client) 
{   
	new weapon = GetPlayerWeaponSlot(client, 0);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType));
}

stock GetSecondaryAmmoHolster(client)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	new iAmmoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	return GetEntData(client, (FindSendPropOffs("CBasePlayer", "m_iAmmo")+4*iAmmoType));
}

stock SetPrimaryAmmoClip(client, value)
{
	new weapon = GetPlayerWeaponSlot(client, 0);
	SetEntProp(weapon, Prop_Data, "m_iClip1", value);
}

stock SetSecondaryAmmoClip(client, value)
{
	new weapon = GetPlayerWeaponSlot(client, 1);
	SetEntProp(weapon, Prop_Data, "m_iClip1", value);
}

stock SetPrimaryAmmoHolster(client, value)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	new weapon = GetPlayerWeaponSlot(client, 0);	
	SetEntData(client, FindSendPropOffs("CBasePlayer", "m_iAmmo")+(GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType")*4), value, 4, true);
}

stock SetSecondaryAmmoHolster(client, value)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	new weapon = GetPlayerWeaponSlot(client, 1);	
	SetEntData(client, FindSendPropOffs("CBasePlayer", "m_iAmmo")+(GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType")*4), value, 4, true);
}

stock SetEntityArmor(client, armor)
{
	SetEntProp(client, Prop_Data, "m_ArmorValue", armor, 4);	
}

stock SetEntitySpeed(client, Float:fspeed)
{
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", fspeed);	
}

stock SetSpeed(client, Float:Speed)
{

	SetEntPropFloat(client, Prop_Send, "m_flMaxspeed", Speed)
}

stock bool:HasClientWeapon(client, const String:WeaponName[], slot)
{
	new weapon = GetPlayerWeaponSlot(client, slot);
	new String:ClassName[64];
	GetEdictClassname(weapon, ClassName, sizeof(ClassName));
	if(StrEqual(ClassName,WeaponName))
	{
		return true;
	}
	return false;
}

stock ClientWeapon(client,String:wName[] , slot)
{
	new weapon = GetPlayerWeaponSlot(client, slot);
	if(IsValidEdict(weapon))
	{
		GetEdictClassname(weapon, wName, 64);
	}
}
/* -------Not yet checked-------
stock GiveWeapon(Client, const String:Stuff[])
{
	
	new String:Buffer[24][64];
	ExplodeString(Stuff, " ", Buffer, 24, 64);
	
	for(new X = 0; X < 23; X++)
	{
		if(!StrEqual(Buffer[X], ""))
		{
			rp_giveplayeritem(Client, Buffer[X])	
		}
	}
	
}
stock WeaponRemove(Client, MaxGuns)
{
	if(!IsClientInGame(Client)) return true;
	
	//Declare:
	decl Offset;
	decl WeaponId;
	
	//Initialize:
	Offset = FindSendPropOffs("CBasePlayer", "m_hMyWeapons");
	
	//Loop:
	for(new X = 0; X < MaxGuns; X = (X + 4))
	{
		
		//Initialize:
		WeaponId = GetEntDataEnt2(Client, Offset + X);
		
		//Valid:
		if(WeaponId > 0)
		{
			
			//Weapon:
			RemovePlayerItem(Client, WeaponId);
			RemoveEdict(WeaponId);
		}
	}
	return true;
}

//Spawns a weapon
stock SpawnWeapon(client, String:weapon[32])
{

	new index = rp_createent(weapon);
	new Float:cllocation[3];
	GetEntPropVector(client, Prop_Send, "m_vecOrigin", cllocation);
	cllocation[2]+=20;
	rp_teleportent(index,cllocation, NULL_VECTOR, NULL_VECTOR);
	rp_dispatchspawn(index);

}

//Gravity Gun:
public DefaultWeapon(Client)
{
	
	//TODO Grabber Mod
}*/