#if defined _rp_building_trainingcenter_access_included
 #endinput
#endif
#define _rp_building_trainingcenter_access_included


enum RP_TrainingcenterAccess
{
	TrainingcenterAccess_PlayerID,
	TrainingcenterAccess_AccessID,
	TrainingcenterAccess_BuildingID
}

enum
{
	Trainingcenter_Access_Manager_Access,
	Trainingcenter_Access_Worker_Access,
}

native RP_GetPlayerTrainingcenterAccess(client, buildingID);
native RP_SetPlayerTrainingcenterAccess(client, buildingID, new_accessID);


public __pl_rp_building_trainingcenter_access_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetPlayerTrainingcenterAccess");
	MarkNativeAsOptional("RP_SetPlayerTrainingcenterAccess");
}