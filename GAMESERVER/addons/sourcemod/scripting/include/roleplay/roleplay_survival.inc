#if defined _rp_survival_included
 #endinput
#endif
#define _rp_survival_included
	
native RP_GetSaturation(client);
native RP_SetSaturation(client, amount);
native RP_AddSaturation(client, amount);
native RP_TakeSaturation(client, amount);
	
native RP_GetStomachContent(client);
native RP_SetStomachContent(client, amount);
native RP_AddStomachContent(client, amount);
native RP_TakeStomachContent(client, amount);
	
native RP_GetHydration(client);
native RP_SetHydration(client, amount);
native RP_AddHydration(client, amount);
native RP_TakeHydration(client, amount);
	
native RP_GetStamina(client);
native RP_SetStamina(client, amount);
native RP_AddStamina(client, amount);
native RP_TakeStamina(client, amount);

public __pl_rp_survival_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetSaturation");
	MarkNativeAsOptional("RP_SetSaturation");
	MarkNativeAsOptional("RP_AddSaturation");
	MarkNativeAsOptional("RP_TakeSaturation");
	
	MarkNativeAsOptional("RP_GetStomachContent");
	MarkNativeAsOptional("RP_SetStomachContent");
	MarkNativeAsOptional("RP_AddStomachContent");
	MarkNativeAsOptional("RP_TakeStomachContent");
	
	MarkNativeAsOptional("RP_GetHydration");
	MarkNativeAsOptional("RP_SetHydration");
	MarkNativeAsOptional("RP_AddHydration");
	MarkNativeAsOptional("RP_TakeHydration");
	
	MarkNativeAsOptional("RP_GetStamina");
	MarkNativeAsOptional("RP_SetStamina");
	MarkNativeAsOptional("RP_AddStamina");
	MarkNativeAsOptional("RP_TakeStamina");
}