#if defined _rp_vendors_included
 #endinput
#endif
#define _rp_vendors_included

enum RP_Vendor
{
	Vendor_ID,
	Vendor_NpcID,
	Vendor_ItemID,
	Float:Vendor_Multiplier
}

forward RP_OnBuyItem(client, itemID);
forward RP_OnSellItem(client, itemID);

native RP_AddVendorItem(npcID, itemID, Float:multi);

native RP_GetVendorCount(vendorID);
native RP_GetVendorIDByIndex(index);
native RP_GetVendorIndexByVendorID(vendorID);

native RP_GetVendorItemCount(vendorID);
native RP_GetVendorItemTypeCount(vendorID);
native RP_GetVendorItemSubTypeCount(vendorID);

native RP_GetVendorNpcID(vendorID);
native RP_GetVendorMultiplier(vendorID, &Float:multiplier);

public __pl_rp_vendors_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_AddVendorItem");
	
	MarkNativeAsOptional("RP_GetVendorCount");
	MarkNativeAsOptional("RP_GetVendorIDByIndex");
	MarkNativeAsOptional("RP_GetVendorIndexByVendorID");
	
	MarkNativeAsOptional("RP_GetVendorItemCount");
	MarkNativeAsOptional("RP_GetVendorItemTypeCount");
	MarkNativeAsOptional("RP_GetVendorItemSubTypeCount");
	
	MarkNativeAsOptional("RP_GetVendorMultiplier");
}