#if defined _rp_npcs_included
 #endinput
#endif
#define _rp_npcs_included

enum 
{
	NT_Other,
	NT_Civilian,
	NT_Vendor,
	NT_Banker,
	NT_Bailmaster,
	NT_Guard,
	NT_Sniper,
	NT_Housemaid
}

enum RP_Npc
{
	Npc_ID,
	Npc_Type,
	String:Npc_Name[64],
	Npc_Team,
	Npc_ReqLevel,
	Npc_ReqItem,
	bool:Npc_VipOnly,
	Npc_Range,
	String:Npc_Model[128],
	String:Npc_Animation[64],
	Float:Npc_Pos[3],
	Float:Npc_Angle,
	Npc_Entity
}

/* Forwards */

forward RP_OnPlayerNpcUse(client, npcID);

/* Natives */

native RP_ReloadNpcs();

native RP_GetNpcType(npcID);
native RP_GetNpcName(npcID, String:buffer[], length_64);

native RP_CreateNPC(type, String:name[64], team, req_level, req_item, vip, range, String:model[128], Float:xpos, Float:ypos, Float:zpos, Float:angel);
native RP_CreateNPCdefault(Float:xpos, Float:ypos, Float:zpos, Float:angel);

native RP_GetNpcReqLevel(npcID);
native RP_GetNpcReqItem(npcID);
native RP_GetNpcVipOnly(npcID);
native RP_GetNpcTeam(npcID);
native RP_GetNpcRange(npcID);

native RP_GetNpcModel(npcID, String:buffer[], length_128);
native RP_GetNpcEntity(npcID);

native RP_SetNpcType(npcID,new_type);
native RP_SetNpcReqLevel(npcID,new_level);
native RP_SetNpcReqItem(npcID,new_item);
native RP_SetNpcVipOnly(npcID,bool:new_vip);
native RP_SetNpcTeam(npcID,new_team);
native RP_SetNpcRange(npcID,new_range);

native RP_SetNpcModel(npcID, String:modelname[]);
native RP_SetNpcName(npcID,String:name[]);

native RP_ResetNpcAnimation(npcID);
native RP_PlayNpcAnimation(npcID, String:animation_64[]);

native RP_GetNpcID(entity);

public __pl_rp_npcs_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_ReloadNpcs");
	
	MarkNativeAsOptional("RP_GetNpcType");
	MarkNativeAsOptional("RP_GetNpcName");
	
	MarkNativeAsOptional("RP_CreateNPC");
	MarkNativeAsOptional("RP_CreateNPCdefault");
	
	MarkNativeAsOptional("RP_GetNpcReqLevel");
	MarkNativeAsOptional("RP_GetNpcReqItem");
	MarkNativeAsOptional("RP_GetNpcVipOnly");
	MarkNativeAsOptional("RP_GetNpcTeam");
	MarkNativeAsOptional("RP_GetNpcRange");
	
	MarkNativeAsOptional("RP_GetNpcModel");
	MarkNativeAsOptional("RP_GetNpcEntity");
	
	MarkNativeAsOptional("RP_SetNpcReqLevel");
	MarkNativeAsOptional("RP_SetNpcReqItem");
	MarkNativeAsOptional("RP_SetNpcVipOnly");
	MarkNativeAsOptional("RP_SetNpcTeam");
	MarkNativeAsOptional("RP_SetNpcRange");
	
	MarkNativeAsOptional("RP_SetNpcModel");
	MarkNativeAsOptional("RP_SetNpcEntity");
	
	MarkNativeAsOptional("RP_ResetNpcAnimation");
	MarkNativeAsOptional("RP_PlayNpcAnimation");
	
	MarkNativeAsOptional("RP_GetGetNpcID");
}