#if defined _rp_players_included
 #endinput
#endif
#define _rp_players_included

enum RP_Player
{
	Player_ID,
	Player_Client,
	String:Player_Auth[32],
	String:Player_Name[64],
	Player_TimeFirstSeen,
	Player_TimeLastSeen,
	Player_TimeConnected
}

forward RP_OnPlayerReady(client, playerID, bool:created);

native RP_GetPlayerID(client);
native RP_GetClient(PlayerID);

native RP_GetName(client, String:buffer[], length_64);
native RP_GetSteamID(client, String:buffer[], length_32);

native RP_GetTimeFirstSeen(client);
native RP_GetTimeConnected(client);
native RP_GetTimeLastSeen(client);

native RP_GetNameByPlayerID(PlayerID, String:buffer[], length_64);
native RP_GetSteamIDByPlayerID(PlayerID, String:buffer[], length_32);

native RP_GetTimeFirstSeenByPlayerID(PlayerID);
native RP_GetTimeLastSeenByPlayerID(PlayerID);
native RP_GetTimeLastDisconnectByPlayerID(PlayerID);

public __pl_rp_players_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetPlayerID");
	MarkNativeAsOptional("RP_GetClient");
	
	MarkNativeAsOptional("RP_GetName");
	MarkNativeAsOptional("RP_GetSteamID");
	MarkNativeAsOptional("RP_GetTimeFirstSeen");
	MarkNativeAsOptional("RP_GetTimeConnected");
	MarkNativeAsOptional("RP_GetTimeLastSeen");
	
	MarkNativeAsOptional("RP_GetNameByPlayerID");
	MarkNativeAsOptional("RP_GetSteamIDByPlayerID");
	MarkNativeAsOptional("RP_GetTimeFirstSeenByPlayerID");
	MarkNativeAsOptional("RP_GetTimeConnectedByPlayerID");
	MarkNativeAsOptional("RP_GetTimeLastSeenByPlayerID");
}