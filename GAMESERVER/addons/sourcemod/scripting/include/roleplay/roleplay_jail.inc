#if defined _rp_jail_included
 #endinput
#endif
#define _rp_jail_included

enum RP_Jail
{
	Jail_ID,
	Jail_Type,
	Jail_PD,
	String:Jail_Name[128],
	Float:Jail_Pos[3],
	Float:Jail_Angle[3]
}

enum RP_PD
{
	PD_ID,
	String:PD_Name,
	PD_Jailcount
}

native RP_CreateJail(client,jtyp,policeDepartment,String:jailname[128]);
native RP_ClientSendToJail(client,jailnr);
native bool:RP_PDHasJailtype(pd,jtype);
native RP_ClientSendToPD(client,type,pd);
native RP_CreatePD(pdid,String:pdname[128]);
native RP_getPDcount();
native RP_GetPDName(nr , String:Buffer[128]);

public __pl_rp_jail_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_CreateJail");
	MarkNativeAsOptional("RP_ClientSendToJail");
	MarkNativeAsOptional("RP_PDHasJailtype");
	MarkNativeAsOptional("RP_ClientSendToPD");
	MarkNativeAsOptional("RP_CreatePD");
	MarkNativeAsOptional("RP_getPDcount");
	MarkNativeAsOptional("RP_GetPDName");
}