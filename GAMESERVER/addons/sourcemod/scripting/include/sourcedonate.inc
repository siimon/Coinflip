#if defined _sourcedonate_included
  #endinput
#endif
#define _sourcedonate_included

#define LoopPackets(%1) for(int %1=0;%1<Sourcedonate_PacketGetCount();++%1)

/* Forwards */

forward void Sourcedonate_OnPlayerLoaded(int client, bool create);

forward void Sourcedonate_OnRunPlayerAdmin(int client, char[] flags, int packetID, char[] packetName, int expire); // expire: timestamp

forward void Sourcedonate_OnRunPlayerCmd(int client, char[] command, int packetID, char[] packetName, int expire); // expire: timestamp

forward bool Sourcedonate_OnRunPlayerCustomCmd(int client, char[] command, int packetID, char[] packetName, int expire); // Confirm if return is true

forward void Sourcedonate_OnPlayerGivenCredits(int client, float amount, char[] reason);

forward void Sourcedonate_OnPlayerSavedCredits(int client);

/* Natives */

native bool Sourcedonate_PlayerIsLoaded(int client);
native float Sourcedonate_PlayerReload(int client);

native float Sourcedonate_PlayerGetCredits(int client);
native float Sourcedonate_PlayerGetCreditsTemp(int client);
native float Sourcedonate_PlayerGetCreditsSpent(int client);
native float Sourcedonate_PlayerGetCreditsDeposit(int client);
native bool Sourcedonate_PlayerGiveCredits(int client, float amount, bool save, bool happy, char[] reason);
native float Sourcedonate_PlayerSaveCredits(int client);

native float Sourcedonate_PlayerHasPacket(int client, int packetId);
native float Sourcedonate_PlayerGivePacket(int client, int packetId);
native float Sourcedonate_PlayerRemovePacket(int client, int packetId);

native int Sourcedonate_GetHappyHourInfo(); //0: disabled, -X: end in (min), +X starts in (min)

native int Sourcedonate_PacketGetCount();
native int Sourcedonate_PacketGetID(int packetIndex);
native bool Sourcedonate_PacketGetName(int packetId, char[] buffer, int size);
native float Sourcedonate_PacketGetPrice(int packetId);
native int Sourcedonate_PacketGetExpire(int packetId); // expire: timestamp

native bool Sourcedonate_PacketHasSale(int packetId); // expire: timestamp
native bool Sourcedonate_PacketGetSale(int packetId, float &newprice, float &oldprice, int &expire); // expire: timestamp

native bool Sourcedonate_PlayerHasFeature(int client, char feature[64]);
native bool Sourcedonate_PlayerGiveFeature(int client, char feature[64]);
native bool Sourcedonate_PlayerRemoveFeature(int client, char feature[64]);

public void __pl_sourcedonate_SetNTVOptional() 
{
	MarkNativeAsOptional("Sourcedonate_PlayerIsLoaded");
	MarkNativeAsOptional("Sourcedonate_PlayerReload");
	MarkNativeAsOptional("Sourcedonate_PlayerGetCredits");
	MarkNativeAsOptional("Sourcedonate_PlayerGetCreditsTemp");
	MarkNativeAsOptional("Sourcedonate_PlayerGetCreditsSpent");
	MarkNativeAsOptional("Sourcedonate_PlayerGetCreditsDeposit");
	MarkNativeAsOptional("Sourcedonate_PlayerGiveCredits");
	MarkNativeAsOptional("Sourcedonate_PlayerSaveCredits");
	MarkNativeAsOptional("Sourcedonate_PlayerHasPacket");
	MarkNativeAsOptional("Sourcedonate_PlayerGivePacket");
	MarkNativeAsOptional("Sourcedonate_PlayerRemovePacket");
	MarkNativeAsOptional("Sourcedonate_IsHappyHour");
	MarkNativeAsOptional("Sourcedonate_PacketGetCount");
	MarkNativeAsOptional("Sourcedonate_PacketGetID");
	MarkNativeAsOptional("Sourcedonate_PacketGetName");
	MarkNativeAsOptional("Sourcedonate_PacketGetPrice");
	MarkNativeAsOptional("Sourcedonate_PacketHasSale");
	MarkNativeAsOptional("Sourcedonate_PacketGetSale");
}