#if defined _warden_included
 #endinput
#endif
#define _warden_included

enum Day {
	k_None=0,
	k_Duck,
	k_Freeday,
	k_Freeze,
	k_HnS,
	k_HG,
	k_Noscope,
	k_Warday,
	k_Zombie,
	k_Dodge,
	k_Knife
};

native Day:JailIsSpecialDay();

